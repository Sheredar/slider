init("slider-wrapper") 
init("second-slider")

function init(parentname) {    
  let parent = document.getElementsByClassName(parentname)[0]

  let px = document.createElement('div')
  parent.appendChild(px)
  px.classList.add('pagination') 

  let slider = parent.children[0]
  let slides = slider.children
  let controls = parent.getElementsByClassName('controls')[0]
  let pagbox = parent.getElementsByClassName('pagination')[0]
  let right = controls.getElementsByClassName('right')[0]
  let left = controls.getElementsByClassName('left')[0]
  let points = pagbox.children
  let e = 0
  let k = 0

  slides[0].classList.add('active')

  pointscreating(slides,pagbox)

  let int = setInterval(function() {    
    goright(slides)
    pointcheck(slides,points)  
  },3000)

  pointcheck(slides,points)

  right.addEventListener('click', function() {
  clearInterval(int)   
  goright(slides)
  pointcheck(slides,points)      
  })

  left.addEventListener('click', function() {
  clearInterval(int)  
  goleft(slides)
  pointcheck(slides,points) 
  })

  pagbox.addEventListener('click', function(e) {
  clearInterval(int) 
  pointschanging(slides,points,e)
  })
}

function pointcheck(slides,points) {
  for (let i = 0; i < slides.length; i++)    
    if (slides[i].classList.value == "slide active") {      
      points[i].classList.add('active')    
      }
    else {points[i].classList.remove('active')}   
}

function goright(slides) {
  for (let i = 0; i < slides.length; i++)
      if (slides[i].classList.value == "slide active")
          k=i;

  if (k<slides.length-1) {
      slides[k].classList.remove("active")
      slides[k+1].classList.add('active')
      k=k+1
    }
    else {      
      slides[k].classList.remove("active")      
      slides[0].classList.add('active')
      k=0      
    }    
}

function goleft(slides) {
  for (let i = 0; i < slides.length; i++)
      if (slides[i].classList.value == "slide active")
          k=i;

  if (k>0) {
      slides[k].classList.remove("active")
      slides[k-1].classList.add('active')
      k=k-1
    }
    else {      
      slides[k].classList.remove("active")      
      slides[slides.length-1].classList.add('active')
      k=slides.length-1      
    }   
}

function pointschanging(slides,points,e) {
  for (let z = 0; z < slides.length; z++) {
    points[z].classList.remove('active')
  }
  e.target.classList.add('active')
  for (let i = 0; i < slides.length; i++)    
    if (points[i].classList.value == "point active") {      
      slides[i].classList.add('active')    
      }
    else {slides[i].classList.remove('active')}  
}

function pointscreating(slides,pagbox) {
  for (let i = 0; i < slides.length; i++) {
    let point = document.createElement('div')
    pagbox.appendChild(point)
    point.classList.add('point')     
    } 
}













